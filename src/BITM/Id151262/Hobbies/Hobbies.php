<?php

namespace App\Hobbies;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class Hobbies extends DB
{

    public $id = "";

    public $name = "";

    public $hobbies = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `hobbies`');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view(){
        $STH = $this->conn->query('SELECT * from hobbies WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->book_title,$this->hobbies);
        $STH = $this->conn->prepare("UPDATE `hobbies` SET `username` = ? , `hobbies` = ? WHERE `id` =".$this->id);
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Been Updated Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('hobbies',$data)){
            $hobby = implode(',',$data['hobbies']);
            $this->hobbies = $hobby;
        }
    }
    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `hobbies` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->hobbies);
        $STH = $DBH->prepare("INSERT INTO `hobbies`(`id`, `username`, `hobbies`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}