<?php

namespace App\Gender;

use App\Database as DB;

use PDO;
use App\Message\Message;
use App\Utility\Utility;
class Gender extends DB
{

    public $id = "";

    public $name = "";

    public $gender = "";


    public function __construct()
    {

        parent::__construct();

    }
    public function index($Mode="ASSOC"){

        $STH = $this->conn->query('SELECT * FROM `gender`');


        if($Mode=="OBJ")   $STH->setFetchMode(PDO::FETCH_OBJ);
        else               $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();

        return $arrAllData;


    }
    public function view(){
        $STH = $this->conn->query('SELECT * from gender WHERE `id`='.$this->id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $objAllData = $STH->fetchAll();
        return $objAllData;
    }

    public function update(){

        $data = array($this->name,$this->gender);
        $STH = $this->conn->prepare("UPDATE `gender` SET `name` =  ? , `gender` = ? WHERE `id` =".$this->id);
        $STH->execute($data);

        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Gender: $this->gender ] <br> Data Has Been Updated Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function setData($data = NULL){
        if(array_key_exists('id',$data)){
            $this->id = $data['id'];
        }
        if(array_key_exists('name',$data)){
            $this->name= $data['name'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender = $data['gender'];
        }

    }
    public function delete(){
        $DBH = $this->conn;
        $STH = $DBH->prepare('DELETE from `gender` WHERE `id`='.$this->id);
        $STH->execute();

        Message::message("<div id='msg'></div><h3 align='center'> <br> Data Has Been Deleted Successfully!</h3></div>");


        Utility::redirect('index.php');
    }

    public function store(){
        $DBH = $this->conn;
        $data = array($this->name,$this->gender);
        $STH = $DBH->prepare("INSERT INTO `gender`(`id`, `name`, `gender`) VALUES (NULL ,?,?)");
        $STH->execute($data);
        Message::message("<div id='msg'></div><h3 align='center'>[ Name: $this->name ] , [ Gender: $this->gender ] <br> Data Has Been Inserted Successfully!</h3></div>");


        Utility::redirect('create.php');


    }


}