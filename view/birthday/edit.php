<?php
//var_dump($_GET);
include_once('../../vendor/autoload.php');
use App\Birthday\Birthday;
use App\Utility\Utility;

$obj= new Birthday();
$obj->setData($_GET);
$oneData = $obj->view();
foreach($oneData as $singleItem) {
    $id = $singleItem->id;
    $name = $singleItem->username;
    $date = $singleItem->birthday;
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birthday</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Birthday</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Birthday:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $id?>">
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter Name" value="<?php echo $name?>">
            <input type="date" name="date" class="form-control" id="date" placeholder="Enter BIRTHDAY" value="<?php echo $date?>">
        <?php } ?>
        </div>

        <button type="submit" value="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
