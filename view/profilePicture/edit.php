<?php
//var_dump($_GET);
include_once('../../vendor/autoload.php');
use App\ProfilePicture\ProfilePicture;
use App\Utility\Utility;

$obj= new ProfilePicture();
$obj->setData($_GET);
$oneData = $obj->view();
foreach($oneData as $singleItem) {
    $id = $singleItem->id;
    $title = $singleItem->username;
    $author = $singleItem->pro_pic;
//Utility::d($singleItem);
?>





<!DOCTYPE html>
<html lang="en">
<head>
    <title>CRUD-BOOK</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Book Title</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label>Edit Book Title:</label>
            <input type="hidden" name="id" id="title" value="<?php echo $id?>">
            <input type="text" name="name" class="form-control" id="title" placeholder="Enter Name" value="<?php echo $title?>">
            <input type="file" name="image" class="form-control" id="author" placeholder="Enter Author Name" value="<?php echo $author?>">
            <?php } ?>
        </div>

        <button type="submit" value="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
